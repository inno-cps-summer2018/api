package com.smef.entity;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AverageMetricsObject {
	private String metricShortName;
	private Double averageValue;
	
	
	public String getMetricShortName() {
		return metricShortName;
	}
	public void setMetricShortName(String metricShortName) {
		this.metricShortName = metricShortName;
	}
	public Double getAverageValue() {
		return averageValue;
	}
	public void setAverageValue(Double averageValue) {
		this.averageValue = averageValue;
	}
	

}
