package com.smef.entity;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Map;

@XmlRootElement
public class Project {
    private Integer id;
    private Long extractionTS;
    private String url;
    private String name;
    private String commitID;
    private Long commitTS;

    public Project(Map<String, String> map) {
        this.setId(map.get("id"));
        this.setExtractionTS(map.get("extractionts"));
        this.setUrl(map.get("url"));
        this.setName(map.get("name"));
        this.setCommitID(map.get("commitid"));
        this.setCommitTS(map.get("committs"));

        System.out.println(this);
    }

    public Project() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setId(String id) {
        this.id = (id == null) ? null : Integer.parseInt(id);
    }

    public String getCommitID() {
        return commitID;
    }

    public void setCommitID(String commitID) {
        this.commitID = commitID;
    }

    public Long getCommitTS() {
        return commitTS;
    }

    public void setCommitTS(long commitTS) {
        this.commitTS = commitTS;
    }
    public void setCommitTS(String commitTS) {
        this.commitTS = (commitTS==null)?null:Long.parseLong(commitTS);
    }

    public Long getExtractionTS() {
        return extractionTS;
    }

    public void setExtractionTS(long extractionTS) {
        this.extractionTS = extractionTS;
    }
    public void setExtractionTS(String extractionTS) {
        this.extractionTS = (extractionTS==null)?null:Long.parseLong(extractionTS);
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}