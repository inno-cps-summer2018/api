package com.smef.entity;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CommitDifferenceObject {
	private String regionName;
	private String  metricShortName;
	private Double value1;
	private Double  value2;
	
	
	public String  getRegionName() {
		return regionName;
	}
	public void setRegionName(String  a) {
		this.regionName = a;
	}
	public String  getMetricShortName() {
		return metricShortName;
	}
	public void setMetricShortName(String  b) {
		this.metricShortName = b;
	}
	public Double getValue1() {
		return value1;
	}
	public void setValue1(Double c) {
		this.value1 = c;
	}
	public Double getValue2() {
		return value2;
	}
	public void setValue2(Double d) {
		this.value2 = d;
	}
	
	
}
