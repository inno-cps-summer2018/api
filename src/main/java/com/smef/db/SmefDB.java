package com.smef.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.smef.entity.MetricStore;
import com.smef.entity.Project;
import com.smef.entity.Region;
import com.smef.entity.Smef;

public class SmefDB extends Database {

    public SmefDB() {
        super();
    }

    private String SQLQueryFromProject(Project project) {
        String pre = "SELECT * FROM project";
        String sql = "";
        if (project.getId() != null)
            sql += "id=" + project.getId() + "  AND ";
        if (project.getExtractionTS() != null)
            sql += "extraction_time_stamp=" + new Timestamp(project.getExtractionTS()) + "  AND ";
        if (project.getUrl() != null)
            sql += "url='" + project.getUrl() + "' AND ";
        if (project.getName() != null)
            sql += "name='" + project.getName() + "' AND ";
        if (project.getCommitID() != null)
            sql += "commit_id='" + project.getCommitID() + "' AND ";
        if (project.getCommitTS() != null)
            sql += "commit_ts=" + new Timestamp(project.getCommitTS()) + "  AND ";

        if (!sql.equals(""))
            sql = " WHERE " + sql.substring(0, sql.length() - 5);

        return pre + sql;
    }

    private String SQLQueryFromRegion(Region region) {
        String pre = "SELECT * FROM region";
        String sql = "";
        if (region.getId() != null)
            sql += "id=" + region.getId() + "  AND ";
        if (region.getParentId() != null)
            sql += "parent_id=" + region.getParentId() + "  AND ";
        if (region.getExtractionTS() != null)
            sql += "extraction_time_stamp=" + new Timestamp(region.getExtractionTS()) + "  AND ";
        if (region.getLanguage() != null)
            sql += "language=" + region.getLanguage() + "  AND ";
        if (region.getType() != null)
            sql += "type=" + region.getType() + "  AND ";
        if (region.getName() != null)
            sql += "name='" + region.getName() + "' AND ";
        if (region.getProjectId() != null)
            sql += "project_id=" + region.getProjectId() + "  AND ";
        if (region.getCommitId() != null)
            sql += "commit_id='" + region.getCommitId() + "' AND ";
        if (region.getCommitTs() != null)
            sql += "commit_ts=" + new Timestamp(region.getCommitTs()) + "  AND ";

        if (!sql.equals(""))
            sql = " WHERE " + sql.substring(0, sql.length() - 5);

        return pre + sql;
    }

    private String SQLQueryFromMetricStore(MetricStore metricStore) {
        String pre = "SELECT * FROM metric_store";
        String sql = "";
        if (metricStore.getId() != null)
            sql += "id=" + metricStore.getId() + " AND ";
        if (metricStore.getMetricCode() != null)
            sql += "metric_code=" + metricStore.getMetricCode() + " AND ";
        if (metricStore.getRegionId() != null)
            sql += "region_id=" + metricStore.getRegionId() + " AND ";
        if (metricStore.getValue() != null)
            sql += "value=" + metricStore.getValue() + " AND ";

        if (!sql.equals(""))
            sql = " WHERE " + sql.substring(0, sql.length() - 4);
        return pre + sql;
    }

    private List<Project> getProjectsByOneProject(Project inputProject) {
        String sql = SQLQueryFromProject(inputProject);
        List<Project> projects = new ArrayList<>();
        try {
            Statement st = getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Project project = new Project();
                project.setId(rs.getInt(1));
                project.setExtractionTS(rs.getTimestamp(2).getTime());
                project.setUrl(rs.getString(3));
                project.setName(rs.getString(4));

                projects.add(project);
            }
        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }
        return projects;
    }

    private List<Region> getRegionsByOneRegion(Region inputRegion) {
        String sql = SQLQueryFromRegion(inputRegion);
        List<Region> regions = new ArrayList<>();
        try {
            Statement st = getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Region region = new Region();

                region.setId(rs.getInt(1));
                region.setParentId(rs.getInt(2));
                region.setExtractionTS(rs.getTimestamp(3).getTime());
                region.setLanguage(rs.getInt(4));
                region.setType(rs.getInt(5));
                region.setName(rs.getString(6));
                region.setProjectId(rs.getInt(7));

                regions.add(region);
            }
        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }
        return regions;
    }

    private List<MetricStore> getMetricStoresByOneMetricStore(MetricStore inputMetricStore) {
        String sql = SQLQueryFromMetricStore(inputMetricStore);
        List<MetricStore> metricStores = new ArrayList<>();
        try {
            Statement st = getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                MetricStore metricStore = new MetricStore();

                metricStore.setId(rs.getInt(1));
                metricStore.setRegionId(rs.getInt(2));
                metricStore.setMetricCode(rs.getInt(3));
                metricStore.setValue(rs.getDouble(4));

                metricStores.add(metricStore);
            }
        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }
        return metricStores;
    }

    private Smef getSmefByOneProject(Project inputProject) {
        Region region = new Region();
        region.setProjectId(inputProject.getId());
        List<Region> regions = getRegionsByOneRegion(region);

        for (Region r : regions) {
            MetricStore metricStore = new MetricStore();
            metricStore.setRegionId(r.getId());
            r.setMetricStoreList(getMetricStoresByOneMetricStore(metricStore));
        }

        return new Smef(inputProject, regions);
    }

    public List<Project> getProjects(List<Project> inputProjects) {
        List<Project> projects = new ArrayList<>();
        for (Project project : inputProjects) {
            projects.addAll(getProjectsByOneProject(project));
        }
        return projects;
    }

    public List<Region> getRegions(List<Region> inputRegions) {
        List<Region> regions = new ArrayList<>();
        for (Region region : inputRegions) {
            regions.addAll(getRegionsByOneRegion(region));
        }
        return regions;
    }

    public List<MetricStore> getMetricStores(List<MetricStore> inputMetricStores) {
        List<MetricStore> metricStores = new ArrayList<>();
        for (MetricStore metricStore : inputMetricStores) {
            metricStores.addAll(getMetricStoresByOneMetricStore(metricStore));
        }
        return metricStores;
    }

    public List<Smef> getSmefsByProjects(List<Project> inputProjects) {
        List<Smef> smefs = new ArrayList<>();
        List<Project> projects = getProjects(inputProjects);
        for (Project project : projects) {
            smefs.add(getSmefByOneProject(project));
        }
        return smefs;
    }

    public void postProject(Project project) {
        String sql = "INSERT INTO project VALUES (?,?,?,?,?,?)";
        try {
            PreparedStatement st = getConnection().prepareStatement(sql);
            st.setInt(1, project.getId());
            st.setTimestamp(2, new Timestamp(project.getExtractionTS()));
            st.setString(3, project.getUrl());
            st.setString(4, project.getName());
            st.setString(5, project.getCommitID());
            st.setTimestamp(6, new Timestamp(project.getCommitTS()));
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }
    }

    public void postRegion(Region region) {
        String sql = "INSERT INTO region VALUES (?,?,?,?,?,?,?)";
        try {

            PreparedStatement st = getConnection().prepareStatement(sql);
            st.setInt(1, region.getId());
            st.setInt(2, region.getParentId());
            st.setTimestamp(3, new Timestamp(region.getExtractionTS()));
            st.setInt(4, region.getLanguage());
            st.setInt(5, region.getType());
            st.setString(6, region.getName());
            st.setInt(7, region.getProjectId());
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }
    }

    public void postMetricStore(MetricStore metricStore) {
        String sql = "INSERT INTO metric_store VALUES (?,?,?,?)";
        try {
            PreparedStatement st = getConnection().prepareStatement(sql);
            st.setInt(1, metricStore.getId());
            st.setInt(2, metricStore.getRegionId());
            st.setInt(3, metricStore.getMetricCode());
            st.setDouble(4, metricStore.getValue());
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }

    }

    public void postSmef(Smef smef) {
        if (!isProjectPresent(smef.getProject())) {
            postProject(smef.getProject());
        }
        for (Region region : smef.getRegions()) {
            if (!isRegionPresent(region)) {
                postRegion(region);
            }
            for (MetricStore metricStore : region.getMetricStoreList()) {
                postMetricStore(metricStore);
            }
        }

    }

    private boolean isMetricStorePresent(MetricStore metricStore) {
        return !getMetricStoresByOneMetricStore(metricStore).isEmpty();
    }

    private boolean isRegionPresent(Region region) {
        return !getRegionsByOneRegion(region).isEmpty();
    }

    private boolean isProjectPresent(Project project) {
        return !getProjectsByOneProject(project).isEmpty();
    }

}