package com.smef.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HttpQueryParser {
    private static final String EQUALS = "=";

    private static Map<String, String> parseSingle(String query, String AND) {
        Map<String, String> map = new HashMap<>();
        if (query.contains(EQUALS)) {
            String[] equations = query.split(AND);
            for (String eq : equations) {
                String[] temp = eq.split(EQUALS);
                temp[0] = temp[0].toLowerCase();
                map.put(temp[0], temp[1]);
            }
        }
        return map;
    }

    public static List<Map<String, String>> parseQuery(String query, String AND, String OR) {

        List<Map<String, String>> mapList = new ArrayList<>();

        String[] singles = query.split(OR);

        for (String s : singles) {
            mapList.add(parseSingle(s, AND));
        }
        return mapList;
    }

}
