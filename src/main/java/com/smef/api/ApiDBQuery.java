package com.smef.api;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.smef.db.QueryDB;
import com.smef.entity.AverageMetricsObject;
import com.smef.entity.CommitDifferenceObject;
import com.smef.entity.Project;
import com.smef.utils.HttpQueryParser;




@Path("dbquery")
public class ApiDBQuery {
	QueryDB db = new QueryDB();

	
	@GET
	@Path("commitDifference")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public List<CommitDifferenceObject> commitDifference(@QueryParam("id1") int id1, @QueryParam("id2") int id2) {
		return db.commitDifference(id1, id2);

	}
	
	@GET
	@Path("averageMetrics")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public List<AverageMetricsObject> averageMetrics(@QueryParam("region") String r, @QueryParam("id1") int id1, @QueryParam("id2") int id2) {
		return db.averageMetrics(r, id1, id2);

	}
	
	

	
}