package com.smef.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.smef.db.SmefDB;
import com.smef.entity.Region;
import com.smef.utils.HttpQueryParser;

@Path("regions")
public class ApiRegion {

    SmefDB db = new SmefDB();

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Region> getRegions() {

        return getRegions("");
    }

    @GET
    @Path("{query}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Region> getRegions(@PathParam("query") String HttpQuery) {
        List<Map<String, String>> paramsList = HttpQueryParser.parseQuery(HttpQuery, "&", ",");

        List<Region> regions = new ArrayList<>();
        for (Map<String, String> map : paramsList) {
            regions.add(new Region(map));
        }

        return db.getRegions(regions);
    }


}
