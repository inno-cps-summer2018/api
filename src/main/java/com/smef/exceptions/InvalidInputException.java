package com.smef.exceptions;
import java.io.Serializable;

public class InvalidInputException extends RuntimeException implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1798247019587650879L;

	/**
	 * 
	 */

	public InvalidInputException() {
		super();
	}
	
	public InvalidInputException(String msg) {
		super(msg);
	}

}
