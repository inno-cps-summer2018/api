package com.smef.exceptions;
import java.io.Serializable;

public class AlreadyExistException extends RuntimeException implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7437337040247482787L;

	public AlreadyExistException() {
		super();
	}
	
	public AlreadyExistException(String msg) {
		super(msg);
	}

}
