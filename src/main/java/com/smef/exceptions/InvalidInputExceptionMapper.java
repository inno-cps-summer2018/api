package com.smef.exceptions;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.*;

@Provider
public class InvalidInputExceptionMapper implements ExceptionMapper<InvalidInputException> {
  
	@Override
	public Response toResponse(InvalidInputException e) {
		return Response.status(Status.BAD_REQUEST)
				       .entity("INVALID INPUT: " + Status.BAD_REQUEST.getReasonPhrase())
					   .build();
	}
}
